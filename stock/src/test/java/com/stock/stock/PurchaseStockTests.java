package com.stock.stock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import com.stock.dto.PurchaseRequestDto;
import com.stock.dto.PurchaseResponseDto;
import com.stock.entity.Customer;
import com.stock.entity.CustomerPurchases;
import com.stock.entity.Stock;
import com.stock.entity.StockType;
import com.stock.exceptions.StocksNotFoundException;
import com.stock.repository.CustomerPurchasesRepository;
import com.stock.repository.CustomerRepository;
import com.stock.repository.StockTypeRepository;
import com.stock.serviceimpl.PurchaseStockServiceImpl;

@SpringBootTest
public class PurchaseStockTests {

	StockType stocktypeone;
	List<StockType> stocktypes;
	Stock stock;

	@Mock
	StockTypeRepository stockTypeRepository;
	@Mock
	CustomerRepository customerRepository;
	@Mock
	CustomerPurchasesRepository customerPurchasesRepository;
	@InjectMocks
	PurchaseStockServiceImpl purchaseStockServiceImpl;
	PurchaseRequestDto purchaseRequestDto;
	PurchaseResponseDto purchaseResponseDto;
	CustomerPurchases customerPurchases;
	Customer customer;

	@BeforeEach
	public void setup() {
		stock = new Stock();
		stock.setStockId(1);
		stocktypeone = new StockType();
		stocktypeone.setQuantity(1000);
		stocktypeone.setUnitCost(10);
		stocktypeone.setStockTypeName("SBI INSURANCE");
		stocktypeone.setStock(stock);
		purchaseRequestDto = new PurchaseRequestDto();
		purchaseRequestDto.setCustomerId(1);
		purchaseRequestDto.setQuntity(5);
		purchaseRequestDto.setStockId(1);
		purchaseRequestDto.setStocTypekName("SBI INSURANCE");
		customerPurchases = new CustomerPurchases();
		customer = new Customer();
		customer.setName("hanuman");

	}

	@Test
	public void purchaseStockNegitiveOne() throws StocksNotFoundException {
		StockType stocktypetwo = null;
		Mockito.when(stockTypeRepository.findByStock_stockIdAndStockTypeNameLike(Mockito.anyInt(), Mockito.anyString()))
				.thenReturn(stocktypetwo);
		assertThrows(StocksNotFoundException.class, () -> purchaseStockServiceImpl.purchaseStock(purchaseRequestDto));

	}

	@Test
	public void purchaseStockNegitiveTwo() throws StocksNotFoundException {
		stocktypeone.setQuantity(2000);
		purchaseRequestDto.setQuntity(50000);
		Mockito.when(stockTypeRepository.findByStock_stockIdAndStockTypeNameLike(Mockito.anyInt(), Mockito.anyString()))
				.thenReturn(stocktypeone);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),
				purchaseStockServiceImpl.purchaseStock(purchaseRequestDto).getBody().getStatuscode());

	}

	@Test
	public void purchaseStockPositive() throws StocksNotFoundException {
		stocktypeone.setQuantity(2000);
		purchaseRequestDto.setQuntity(500);
		customerPurchases.setCustomer(customer);
		customerPurchases.setQuantity(500);
		customerPurchases.setStockName("SBI INSURANCE");
		customerPurchases.setUnitCost(10);
		customerPurchases.setTotalamount(5000);
		customerPurchases.setDate(LocalDateTime.now());
		customerPurchasesRepository.save(customerPurchases);
		Mockito.when(stockTypeRepository.findByStock_stockIdAndStockTypeNameLike(Mockito.anyInt(), Mockito.anyString()))
				.thenReturn(stocktypeone);
		Mockito.when(customerPurchasesRepository.save(customerPurchases)).thenReturn(customerPurchases);
		Mockito.when(stockTypeRepository.save(stocktypeone)).thenReturn(stocktypeone);

		assertEquals(HttpStatus.OK.value(),
				purchaseStockServiceImpl.purchaseStock(purchaseRequestDto).getBody().getStatuscode());

	}
}
