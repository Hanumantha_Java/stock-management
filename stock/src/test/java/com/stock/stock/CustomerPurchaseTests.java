package com.stock.stock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.stock.entity.Customer;
import com.stock.entity.CustomerPurchases;

import com.stock.exceptions.NoRecorsFoundException;

import com.stock.repository.CustomerPurchasesRepository;
import com.stock.repository.CustomerRepository;
import com.stock.serviceimpl.CustomerServiceImpl;

@SpringBootTest
public class CustomerPurchaseTests {
	@Mock
	CustomerRepository customerRepository;
	@Mock
	CustomerPurchasesRepository customerPurchasesRepository;
	@InjectMocks
	CustomerServiceImpl customerServiceImpl;
	CustomerPurchases customerPurchases;
	Customer customer;
	List<CustomerPurchases> customerResponseDto;

	@BeforeEach
	public void setUp() {
		customer = new Customer();
		customer.setAddrees("hyderabad");
		customer.setEmail("hanumantha@gmail.com");
		customer.setMobile("76869898976");
		customer.setName("hanuman");
		customerPurchases = new CustomerPurchases();
		customerPurchases.setCustomer(customer);
		customerPurchases.setQuantity(10);
		customerPurchases.setStockName("sbi insurance");
		customerPurchases.setUnitCost(10);
		customerPurchases.setDate(LocalDateTime.now());
		customerPurchases.setCustomer(customer);
	}

	@Test
	public void getPurchasestocksnegitive() throws NoRecorsFoundException {
		int customerId = 1;
		Mockito.when(customerPurchasesRepository.findAllByCustomer_customerId(customerId))
				.thenReturn(customerResponseDto);
		assertThrows(NoRecorsFoundException.class, () -> customerServiceImpl.findCustomerpuchasedStocks(customerId));

	}

	@Test
	public void getPurchasestocksPositive() throws NoRecorsFoundException {
		int customerId = 1;
		List<CustomerPurchases> customerResponseDto = new ArrayList<>();
		customerResponseDto.add(customerPurchases);
		Mockito.when(customerPurchasesRepository.findAllByCustomer_customerId(Mockito.anyInt()))
				.thenReturn(customerResponseDto);
		assertEquals(1, customerServiceImpl.findCustomerpuchasedStocks(customerId).size());
	}
}
