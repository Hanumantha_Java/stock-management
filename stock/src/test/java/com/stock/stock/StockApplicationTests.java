package com.stock.stock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.stock.entity.Stock;
import com.stock.entity.StockType;
import com.stock.exceptions.StocksNotFoundException;
import com.stock.repository.StockTypeRepository;
import com.stock.serviceimpl.StockServiceImpl;

@SpringBootTest
class StockApplicationTests {

	StockType stocktypeone;
	List<StockType> stocktypes;
	Stock stock;

	@Mock
	StockTypeRepository stockTypeRepository;
	@InjectMocks
	StockServiceImpl stockServiceImpl;

	@BeforeEach
	public void setUp() {
		stock = new Stock();
		stock.setStockId(1);
		stocktypeone = new StockType();
		stocktypeone.setQuantity(1000);
		stocktypeone.setUnitCost(10);
		stocktypeone.setStockTypeName("SBI INSURANCE");
		stocktypeone.setStock(stock);
	}

	@Test
	public void getstocksnegitive() throws StocksNotFoundException {
		int stcokId = 1;
		Mockito.when(stockTypeRepository.findAllByStock_stockId(Mockito.anyInt())).thenReturn(stocktypes);
		assertThrows(StocksNotFoundException.class, () -> stockServiceImpl.getAllStocks(stcokId));
	}

	@Test
	public void getstocksPositive() throws StocksNotFoundException {
		int stcokId = 1;
		List<StockType> stocktypes = new ArrayList<StockType>();
		stocktypes.add(stocktypeone);
		Mockito.when(stockTypeRepository.findAllByStock_stockId(Mockito.anyInt())).thenReturn(stocktypes);
		assertEquals(1, stockServiceImpl.getAllStocks(stcokId).getBody().size());
	}
}
