package com.stock.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stock.dto.CustomerPurchaseDto;
import com.stock.dto.PurchaseRequestDto;
import com.stock.dto.PurchaseResponseDto;
import com.stock.dto.StockTypeDto;
import com.stock.exceptions.NoRecorsFoundException;
import com.stock.exceptions.StocksNotFoundException;
import com.stock.services.CustomerService;
import com.stock.services.PurchaseStockSerive;
import com.stock.services.StockService;

@RequestMapping("stockmanagement")
@RestController
public class StockController {

	@Autowired
	StockService stockService;
	@Autowired
	PurchaseStockSerive purchaseStockSerive;
	@Autowired
	CustomerService customerService;
	Logger logger = LoggerFactory.getLogger(StockController.class);

	/*
	 * @param(stockid)
	 */
	@GetMapping("/stocks/{stockid}")

	public ResponseEntity<List<StockTypeDto>> getAllStocksbystockId(@PathVariable int stockid) throws StocksNotFoundException {
		logger.debug("dugging point at controller method getAllStocksbystockId");
		return stockService.getAllStocks(stockid);
	}

	/*
	 * @param(customerId,stockId,stocTypekName,quntity)
	 */
	@PostMapping("purchases")
	public ResponseEntity<PurchaseResponseDto> purchaseStocksbyid(@Valid @RequestBody PurchaseRequestDto purchaseRequestDto)
			throws StocksNotFoundException {
		logger.debug("dugging point at controller method purchaseStocksbyid");

		return purchaseStockSerive.purchaseStock(purchaseRequestDto);
	}

	/*
	 * @param(customerid)
	 */
	@GetMapping("/customers/{customerid}")
	public List<CustomerPurchaseDto> findCustomerpuchasedStocks(@PathVariable int customerid)
			throws NoRecorsFoundException {
		logger.debug("dugging point at controller method findCustomerpuchasedStocks");
		return customerService.findCustomerpuchasedStocks(customerid);
	}

}
