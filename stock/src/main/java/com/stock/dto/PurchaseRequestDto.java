package com.stock.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PurchaseRequestDto {

	@NotNull(message="customerid should not be empty")
	private Integer customerId;
	@NotNull(message="stockid should not be empty")
	private Integer stockId;
	@NotEmpty(message="stockTypeName should not be empty")
	private String stocTypekName;
	@NotNull(message="quntity should not be empty")
	private Integer quntity;

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getStockId() {
		return stockId;
	}

	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}

	public String getStocTypekName() {
		return stocTypekName;
	}

	public void setStocTypekName(String stocTypekName) {
		this.stocTypekName = stocTypekName;
	}

	public Integer getQuntity() {
		return quntity;
	}

	public void setQuntity(Integer quntity) {
		this.quntity = quntity;
	}

	public PurchaseRequestDto() {
		// create object
	}

}
