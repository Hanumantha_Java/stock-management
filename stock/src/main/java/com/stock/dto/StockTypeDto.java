package com.stock.dto;

public class StockTypeDto {
	
	private int stockTypeId;
	private String stockTypeName;
	private int quantity;
	private double unitCost;
	public int getStockTypeId() {
		return stockTypeId;
	}
	public void setStockTypeId(int stockTypeId) {
		this.stockTypeId = stockTypeId;
	}
	public String getStockTypeName() {
		return stockTypeName;
	}
	public void setStockTypeName(String stockTypeName) {
		this.stockTypeName = stockTypeName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(double unitCost) {
		this.unitCost = unitCost;
	}
	

}
