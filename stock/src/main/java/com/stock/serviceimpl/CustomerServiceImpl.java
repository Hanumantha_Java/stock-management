package com.stock.serviceimpl;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stock.dto.CustomerPurchaseDto;
import com.stock.entity.CustomerPurchases;
import com.stock.exceptions.NoRecorsFoundException;
import com.stock.repository.CustomerPurchasesRepository;
import com.stock.services.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerPurchasesRepository customerPurchasesRepository;
	Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	public List<CustomerPurchaseDto> findCustomerpuchasedStocks(int customerId) throws NoRecorsFoundException {
		List<CustomerPurchaseDto> customerpurchaseDto ;
		logger.info("in CustomerServiceImpl class");
		List<CustomerPurchases> customerResponseDto = customerPurchasesRepository
				.findAllByCustomer_customerId(customerId);

		if (Objects.nonNull(customerResponseDto) && !customerResponseDto.isEmpty()) {

				customerpurchaseDto=customerResponseDto.stream().map(customerresponse->{
					CustomerPurchaseDto customer = new CustomerPurchaseDto();
				customer.setDate(customerresponse.getDate());
				customer.setPurchaseId(customerresponse.getPurchaseId());
				customer.setQuantity(customerresponse.getQuantity());
				customer.setStockName(customerresponse.getStockName());
				customer.setTotalamount(customerresponse.getTotalamount());
				customer.setUnitCost(customerresponse.getUnitCost());
			     return customer;
				}).collect(Collectors.toList());
			
			return customerpurchaseDto;
		} else {
			logger.error("No Customer purchased stocks");
			throw new NoRecorsFoundException("No Customer purchased stocks");

		}

	}

}
