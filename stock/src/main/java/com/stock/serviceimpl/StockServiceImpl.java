package com.stock.serviceimpl;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.stock.dto.StockTypeDto;
import com.stock.entity.StockType;
import com.stock.exceptions.StocksNotFoundException;
import com.stock.repository.StockTypeRepository;
import com.stock.services.StockService;

@Service
public class StockServiceImpl implements StockService {

	@Autowired
	StockTypeRepository stockTypeRepository;
	Logger logger = LoggerFactory.getLogger(StockServiceImpl.class);

	public ResponseEntity<List<StockTypeDto>> getAllStocks(int stcokId) throws StocksNotFoundException {
		logger.info("in StockServiceImpl class ");
		List<StockTypeDto> stockTypeDto;

		List<StockType> allStocks = stockTypeRepository.findAllByStock_stockId(stcokId);

		if (Objects.nonNull(allStocks) && !allStocks.isEmpty()) {
			stockTypeDto = allStocks.stream().map(customerresponse -> {
				StockTypeDto stocktype = new StockTypeDto();
				stocktype.setQuantity(customerresponse.getQuantity());
				stocktype.setStockTypeId(customerresponse.getStockTypeId());
				stocktype.setStockTypeName(customerresponse.getStockTypeName());
				stocktype.setUnitCost(customerresponse.getUnitCost());
				return stocktype;
			}).collect(Collectors.toList());
			return new ResponseEntity<>(stockTypeDto, HttpStatus.OK);
		} else {
			logger.error("no stocks available");
			throw new StocksNotFoundException("No stocks available");
		}
	}

}
