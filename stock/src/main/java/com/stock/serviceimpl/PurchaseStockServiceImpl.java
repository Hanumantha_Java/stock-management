package com.stock.serviceimpl;

import java.time.LocalDateTime;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.stock.dto.PurchaseRequestDto;
import com.stock.dto.PurchaseResponseDto;
import com.stock.entity.Customer;
import com.stock.entity.CustomerPurchases;
import com.stock.entity.StockType;
import com.stock.exceptions.StocksNotFoundException;
import com.stock.repository.CustomerPurchasesRepository;
import com.stock.repository.CustomerRepository;
import com.stock.repository.StockTypeRepository;
import com.stock.services.PurchaseStockSerive;

@Service
public class PurchaseStockServiceImpl implements PurchaseStockSerive {

	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	StockTypeRepository stockTypeRepository;
	@Autowired
	CustomerPurchasesRepository customerPurchasesRepository;

	Logger logger = LoggerFactory.getLogger(PurchaseStockServiceImpl.class);

	public ResponseEntity<PurchaseResponseDto> purchaseStock(PurchaseRequestDto purchaseRequestDto)
			throws StocksNotFoundException {
		logger.debug("in PurchaseStockServiceImpl class");
		PurchaseResponseDto purchaseResponseDto = new PurchaseResponseDto();
		CustomerPurchases customerPurchases = new CustomerPurchases();
		StockType stocktype = stockTypeRepository.findByStock_stockIdAndStockTypeNameLike(
				purchaseRequestDto.getStockId(), "%" + purchaseRequestDto.getStocTypekName() + "%");
		Customer customer = customerRepository.findByCustomerId(purchaseRequestDto.getCustomerId());

		if (Objects.nonNull(stocktype)) {

			if (stocktype.getQuantity() >= purchaseRequestDto.getQuntity()) {
				

					stocktype.setQuantity(stocktype.getQuantity() - purchaseRequestDto.getQuntity());
                    customerPurchases.setCustomer(customer);
					customerPurchases.setQuantity(purchaseRequestDto.getQuntity());
					customerPurchases.setStockName(purchaseRequestDto.getStocTypekName());
					customerPurchases.setUnitCost(stocktype.getUnitCost());
					customerPurchases.setTotalamount(((stocktype.getUnitCost()) * (purchaseRequestDto.getQuntity())));
					customerPurchases.setDate(LocalDateTime.now());
					customerPurchasesRepository.save(customerPurchases);
					stockTypeRepository.save(stocktype);
					purchaseResponseDto.setStatuscode(HttpStatus.OK.value());
					purchaseResponseDto.setMessage("purchased stocks successfully");
					return new ResponseEntity<>(purchaseResponseDto, HttpStatus.OK);
				
			} else {
				purchaseResponseDto.setStatuscode(HttpStatus.INTERNAL_SERVER_ERROR.value());
				purchaseResponseDto.setMessage("Stock quantity is not avaiable");
				return new ResponseEntity<>(purchaseResponseDto, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			logger.error("there is no stocks based on stock name");
			throw new StocksNotFoundException("there is no stocks based on stock name");
		}
	}
}
