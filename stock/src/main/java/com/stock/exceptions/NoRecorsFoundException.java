package com.stock.exceptions;

public class NoRecorsFoundException extends Exception {

	private static final long serialVersionUID = -9079454849611061074L;

	public NoRecorsFoundException() {
		super();
	}

	public NoRecorsFoundException(final String message) {
		super(message);
	}

}
