package com.stock.exceptions;

public class StocksNotFoundException extends Exception {

	private static final long serialVersionUID = -9079454849611061074L;

	public StocksNotFoundException() {
		super();
	}

	public StocksNotFoundException(final String message) {
		super(message);
	}

}
