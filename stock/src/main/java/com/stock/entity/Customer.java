package com.stock.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class Customer implements Serializable {

	private static final long serialVersionUID = 145080860L;

	@Id
	@GeneratedValue
	private int customerId;
	@Size(min = 6, max = 12, message = "length should be betwwen in 6 to 12 ")
	private String name;
   @Email(regexp = "^(.+)@(.+)$", message = "invalid email pattern")
	private String email;
	@Pattern(regexp = "[6-9][0-9] {9}", message = "invalid moible number")
	@Size(min = 10, max = 10, message = "digits should be 10")
	private String mobile;
	private String addrees;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddrees() {
		return addrees;
	}

	public void setAddrees(String addrees) {
		this.addrees = addrees;
	}

	public Customer() {
//create object
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
	private Set<CustomerPurchases> customerpurchase;

	public Customer(Set<CustomerPurchases> customerpurchase) {
		super();
		this.customerpurchase = customerpurchase;
	}

	public Set<CustomerPurchases> getCustomerpurchase() {
		return customerpurchase;
	}

	public void setCustomerpurchase(Set<CustomerPurchases> customerpurchase) {
		this.customerpurchase = customerpurchase;
	}
}
