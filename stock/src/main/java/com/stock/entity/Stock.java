package com.stock.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Stock implements Serializable {

	private static final long serialVersionUID = 180860L;

	@Id
	@GeneratedValue
	private int stockId;
	private String name;
	private String description;
	private String country;

	public int getStockId() {
		return stockId;
	}

	public void setStockId(int stockId) {
		this.stockId = stockId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Stock() {
		// create object
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "stock")
	private Set<StockType> stockType;

	public Set<StockType> getStockType() {
		return stockType;
	}

	public void setStockType(Set<StockType> stockType) {
		this.stockType = stockType;
	}

	public Stock(Set<StockType> stockType) {
		super();
		this.stockType = stockType;
	}

}
