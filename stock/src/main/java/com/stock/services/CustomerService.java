package com.stock.services;

import java.util.List;



import com.stock.dto.CustomerPurchaseDto;
import com.stock.exceptions.NoRecorsFoundException;


public interface CustomerService {
	public List<CustomerPurchaseDto> findCustomerpuchasedStocks(int customerId) throws NoRecorsFoundException;

}
