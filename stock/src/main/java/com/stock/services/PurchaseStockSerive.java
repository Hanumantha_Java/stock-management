package com.stock.services;

import org.springframework.http.ResponseEntity;

import com.stock.dto.PurchaseRequestDto;
import com.stock.dto.PurchaseResponseDto;
import com.stock.exceptions.StocksNotFoundException;


public interface PurchaseStockSerive {
	public ResponseEntity<PurchaseResponseDto> purchaseStock(PurchaseRequestDto purchaseRequestDto)
			throws StocksNotFoundException;
}
