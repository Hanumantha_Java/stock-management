package com.stock.services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.stock.dto.StockTypeDto;
import com.stock.exceptions.StocksNotFoundException;

public interface StockService {
	
	public ResponseEntity<List<StockTypeDto>> getAllStocks(int stcokId) throws StocksNotFoundException;

}
