package com.stock.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.stock.entity.CustomerPurchases;

public interface CustomerPurchasesRepository extends JpaRepository<CustomerPurchases, Integer>{

	List<CustomerPurchases> findAllByCustomer_customerId(int customerId);

}

