package com.stock.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stock.entity.StockType;

public interface StockTypeRepository extends JpaRepository<StockType, Integer>{

	List<StockType> findAllByStock_stockId(int stcokId);

	StockType findByStock_stockIdAndStockTypeNameLike(int stockId,String stocTypekName);

}

